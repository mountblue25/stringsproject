
function getMonthName(dateString) {
    let dateArray = dateString.split("/")
    let months = { "1": "Jan", "2": "Feb", "3": "Mar", "4": "April", "5": "May", "6": "June", "7": "July", "8": "Aug", "9": "Sept", "10": "Oct", "11": "Nov", "12": "Dec" }
    if (parseInt(dateArray[1]) <= 12){
        return months[dateArray[1]]
    } else{
        return "Please Provide valid month"
    }
}

module.exports = getMonthName;