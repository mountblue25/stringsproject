
function getFullName(nameObject){
    let result = Object.entries(nameObject).reduce((accumilator, current) => {
        accumilator += current[1] + " "
        return accumilator
    },"")
    return result
}

module.exports = getFullName;