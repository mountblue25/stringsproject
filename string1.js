
function stringsToNumerics(valuesArray){
    let output = valuesArray.map((number) => {
        let value = number.replace("$", "")
        .replace(",","")
        if (isNaN(parseFloat(value)) || !(/^[0-9.-]*$/.test(value))){
            return 0
        } else{
            return parseFloat(value)
        }
    })
    return output
}
module.exports = stringsToNumerics;