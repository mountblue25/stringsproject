
function splittingOfstring(ipAddress){
    let ipArray = ipAddress.split('.')
    let result = ipArray.map((value) => {
        if (/^[0-9]*$/.test(value)){
            return parseInt(value)
        } else{
            return NaN
        }
    })
    if (ipArray.length > 4){
        return []
    }
    else if (result.includes(NaN)){
        return []
    }
    else {
        return result
    }
}
module.exports = splittingOfstring;